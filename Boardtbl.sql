-- 게시판 테이블 생성
CREATE TABLE board (
    board_num NUMBER(4) PRIMARY KEY,
    title VARCHAR2(100),
    content VARCHAR2(1000)
);
COMMENT ON COLUMN board.board_num IS 'Index';
COMMENT ON COLUMN board.title IS '제목';
COMMENT ON COLUMN board.content IS '내용';

-- 게시판 시퀀스 생성
CREATE SEQUENCE board_seq
       INCREMENT BY 1
       START WITH 1
       MINVALUE 1
       MAXVALUE 9999
       NOCYCLE
       NOCACHE
       NOORDER;
       
-- 게시글 
INSERT INTO board VALUES(board_seq.nextval, '첫번째 글', '플로우컨트롤 최고 만만세');
INSERT INTO board VALUES(board_seq.nextval, '두번째 글', '자나깨나 코로나 조심 자가격리는 심심해');
INSERT INTO board VALUES(board_seq.nextval, '세번째 글', '점심은 오리양념고기였다 맛있었다');

commit;
SELECT * from board;