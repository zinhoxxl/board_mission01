package com.board.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.board.service.Sch02Job;


@WebServlet("/Sch02RunServlet")
public class Sch02RunServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private SchedulerFactory schedulerFactory;
	private Scheduler scheduler;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		try {
			schedulerFactory = new StdSchedulerFactory();
			scheduler = schedulerFactory.getScheduler();
			scheduler.start();

			JobDetail job2 = JobBuilder.newJob(Sch02Job.class)
					.withIdentity("job3", "group3")
					.build();

			Trigger trigger2 = (Trigger) TriggerBuilder.newTrigger() 
					.withIdentity("trigger4", "group4") 
					.withSchedule(SimpleScheduleBuilder.simpleSchedule() 
							     					   .withIntervalInSeconds(30)
							     					   .repeatForever()) 
					.startAt(new Date(System.currentTimeMillis() + 30000)) 
					.build();

			scheduler.scheduleJob(job2, trigger2);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
