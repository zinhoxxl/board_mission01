package com.board.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.google.gson.Gson;

@WebServlet("/LineChartServlet")
public class LineChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LineChartServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		BoardDAO dao = BoardDAO.getInstance();
		HashMap<String, Object> hashMap = null;
		try {
			hashMap = dao.lineChart();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		String json = gson.toJson(hashMap);
		response.getWriter().write(json.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
