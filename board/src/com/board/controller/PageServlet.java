package com.board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.service.BoardDAO;
import com.board.vo.PageVO;
import com.google.gson.Gson;

@WebServlet("/PageServlet")
public class PageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PageServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int currentPage = Integer.parseInt(request.getParameter("currentPage"));
		String searchField = request.getParameter("searchField");
		String searchText = null;
		if (request.getParameter("searchText") != null) {
			searchText = request.getParameter("searchText");
		}

		BoardDAO dao = BoardDAO.getInstance();
		int pageCount = 0;
		try {
			pageCount = dao.pageCount(currentPage, searchField, searchText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int totalCount = pageCount;
		int totalPage = 0;
		int startPage = 0;
		int endPage;
		int pageSize = 10;
		int pageRow = 10;
		boolean pageNext;
		boolean pagePrev;

		totalPage = (int) Math.ceil(totalCount / (double) pageRow);
		endPage = ((int) Math.ceil(currentPage / (double) pageSize)) * pageSize;
		startPage = (endPage - pageSize) + 1;

		if (totalPage < endPage) {
			endPage = totalPage;
		}

		if (currentPage < endPage) {
			pageNext = true;
		} else
			pageNext = false;
		if (currentPage <= 1) {
			pagePrev = false;
		} else
			pagePrev = true;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		PageVO vo = new PageVO();
		vo.setCurrentPage((int) currentPage);
		vo.setTotalPage(pageCount);
		vo.setStartPage(startPage);
		vo.setEndPage(endPage);
		vo.setPageSize(pageSize);
		vo.setPageRow(pageRow);
		vo.setPageNext(pageNext);
		vo.setPagePrev(pagePrev);

		Gson gson = new Gson();
		String json = gson.toJson(vo);
		response.getWriter().write(json.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
