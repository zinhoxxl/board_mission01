package com.board.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.board.db.DBConnection;

public class Sch01Job implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException { 
		
		System.out.println("Scheduler 01 20sec : " + new Date(System.currentTimeMillis()));
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String type1 = "Sch#1";
		String type1_Time = dateFormat.format(date);
		
		Connection conn = null;
		PreparedStatement pstmt = null;		
		String SQL = "INSERT INTO board(board_num, title, content) VALUES(board_seq.nextval, ?, ?)";
			try {
				conn = DBConnection.getConnection();				
				pstmt = conn.prepareStatement(SQL);
				pstmt.setString(1, type1);
				pstmt.setString(2, type1_Time);
				pstmt.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					DBConnection.uidClose(conn, pstmt);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}

}
