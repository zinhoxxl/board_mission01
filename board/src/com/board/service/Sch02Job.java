package com.board.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.board.db.DBConnection;

public class Sch02Job implements Job {
	public void execute(JobExecutionContext context) throws JobExecutionException { 
		
		System.out.println("Scheduler 02 30sec : " + new Date(System.currentTimeMillis()));
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String type2_Time = dateFormat.format(date);
		
		Connection conn = null;
		PreparedStatement pstmt = null;		
		String SQL = "UPDATE board SET title = 'Sch#2', content = ? WHERE title = 'Sch#1'";
			try {
				conn = DBConnection.getConnection();				
				pstmt = conn.prepareStatement(SQL);
				pstmt.setString(1, type2_Time);
				pstmt.executeUpdate();				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					DBConnection.uidClose(conn, pstmt);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}

}
