package com.board.service;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import com.board.db.DBConnection;
import com.board.vo.BoardVO;

public class BoardDAO {

	private BoardDAO() {
	}

	private static BoardDAO instance;

	public static BoardDAO getInstance() {
		if (instance == null)
			instance = new BoardDAO();
		return instance;
	}

	/*
	 * 게시글 list, search
	 */
	public ArrayList<BoardVO> list(int currentPage, String searchText, String searchField) throws Exception {

		ArrayList<BoardVO> voList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT * FROM ";
		SQL += "(SELECT ROWNUM AS RNUM, A.board_num, A.title, A.content, A.writeDate ";
		SQL += "FROM ( SELECT board_num, title, content, writeDate FROM board ";
		if ("title".equals(searchField) && !"".equals(searchText)) {
			SQL += "WHERE title LIKE '%" + searchText + "%' ";
		}
		if ("content".equals(searchField) && !"".equals(searchText)) {
			SQL += "WHERE content LIKE '%" + searchText + "%' ";
		}
		SQL += "ORDER BY board_num DESC) A) WHERE RNUM BETWEEN ? AND ?";
		try {
			int num = 1;
			int startPage = (currentPage - 1) * 10 + 1;
			int endPage = currentPage * 10;
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(num++, startPage);
			pstmt.setInt(num++, endPage);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				BoardVO vo = new BoardVO();
				vo.setBoard_num(rs.getInt(2));
				vo.setTitle(rs.getString(3));
				vo.setContent(rs.getString(4));
				vo.setWriteDate(rs.getString(5));
				voList.add(vo);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
		return voList;
	}

	/*
	 * 게시글 등록하기
	 */
	public void insert(BoardVO vo) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "INSERT INTO board(board_num, title, content, fileName, filePath) "
				+ " VALUES(board_seq.nextval,?,?,?,?)";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, vo.getTitle());
			pstmt.setString(2, vo.getContent());
			pstmt.setString(3, vo.getFileName());
			pstmt.setString(4, vo.getFilePath());
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	/*
	 * 게시글 상세보기
	 */
	public BoardVO load(int board_num) throws Exception {

		BoardVO vo = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT * FROM board WHERE board_num = ?";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, board_num);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				vo = new BoardVO();
				vo.setBoard_num(rs.getInt("board_num"));
				vo.setTitle(rs.getString("title"));
				vo.setContent(rs.getString("content"));
				vo.setFileName(rs.getString("fileName"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.selectClose(conn, pstmt, rs);
			} catch (Exception e) {
				throw e;
			}
		}
		return vo;
	}

	/*
	 * 게시글 수정 하기
	 */
	public BoardVO update(BoardVO vo) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "UPDATE board SET title = ?, content = ?, fileName = ?, filePath = ? WHERE board_num = ?";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setString(1, vo.getTitle());
			pstmt.setString(2, vo.getContent());
			pstmt.setString(3, vo.getFileName());
			pstmt.setString(4, vo.getFilePath());
			pstmt.setInt(5, vo.getBoard_num());
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
		return vo;
	}

	/*
	 * 게시글 삭제 하기
	 */
	public void delete(int board_num) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		String SQL = "DELETE FROM board WHERE board_num = ?";
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			pstmt.setInt(1, board_num);
			pstmt.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
	}

	
	/*
	 * 게시글 페이징
	 */
	public int pageCount(int currentPage, String searchField, String searchText) throws Exception {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT COUNT(*) AS CNT FROM board ";
		if ("title".equals(searchField) && !"".equals(searchText)) {
			SQL += "WHERE title like '%" + searchText + "%' ";
		}
		if ("content".equals(searchField) && !"".equals(searchText)) {
			SQL += "WHERE content like '%" + searchText + "%' ";
		}
		int count = 0;
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				count = rs.getInt("CNT");
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
		return count;
	}

	
	/*
	 * 첨부파일 게시물 차트
	 */
	public HashMap<String, Object> pieChart() throws Exception {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT attachmentsTotal, attachments, attachmentsTotal - attachments AS noneAttachments " 
		           + "FROM ( "
				   + "SELECT count(fileName) as attachments, count(*) as attachmentsTotal " 
		           + "FROM board "
				   + "WHERE writeDate >= to_char(add_months(sysdate,-1), 'yyyy-mm-dd') " 
		           + ")";
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				hashMap.put("attachmentsTotal", rs.getInt("attachmentsTotal"));
				hashMap.put("attachments", rs.getInt("attachments"));
				hashMap.put("noneAttachments", rs.getInt("noneAttachments"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
		return hashMap;
	}

	
	/*
	 * 게시물 등록 추이 차트
	 * */
	public HashMap<String, Object> lineChart() throws Exception {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String SQL = "SELECT weeks, count(weeks) AS count " 
				   + "FROM ( " 
				   + "SELECT CASE WHEN writeDate >= SYSDATE-7 THEN 'oneWeeks' " 
				   + "WHEN writeDate BETWEEN SYSDATE-14 AND SYSDATE-7 THEN 'twoWeeks' " 
				   + "WHEN writeDate BETWEEN SYSDATE-21 AND SYSDATE-14 THEN 'threeWeeks' " 
				   + "WHEN writeDate BETWEEN SYSDATE-28 AND SYSDATE-21 THEN 'fourWeeks' " 
				   + "WHEN writeDate BETWEEN SYSDATE-35 AND SYSDATE-28 THEN 'fiveWeeks' " 
				   + "WHEN writeDate BETWEEN SYSDATE-42 AND SYSDATE-35 THEN 'sixWeeks' " 
				   + "ELSE '0' END AS weeks " 
				   + "FROM board ) "
				   + "GROUP BY "
				   + "weeks order by decode "
				   + "(weeks, 'oneWeeks', 1, 'twoWeeks', 2, 'threeWeeks', 3, 'fourWeeks', 4, 'fiveWeeks', 5,'sixWeeks', 6) desc "; 
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		try {
			conn = DBConnection.getConnection();
			pstmt = conn.prepareStatement(SQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				hashMap.put(rs.getString("weeks"), rs.getInt("count"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				DBConnection.uidClose(conn, pstmt);
			} catch (Exception e) {
				throw e;
			}
		}
		return hashMap;
	}

}
