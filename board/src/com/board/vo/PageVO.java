package com.board.vo;

public class PageVO {
	
	private int currentPage;     // 현재 
	private int totalPage;	 	 // 전체 
	private int startPage; 	 	 // 시작
	private int endPage;	 	 // 끝
	private int pageSize;    	 // 보여줄 페이지 번호 갯수
	private int pageRow;	 	 // 페이지 하나에 나오는 게시글 행
	private boolean pageNext;    // 다음페이지
	private boolean pagePrev;	 // 이전페이지
	
	
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public int getStartPage() {
		return startPage;
	}
	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}
	public int getEndPage() {
		return endPage;
	}
	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageRow() {
		return pageRow;
	}
	public void setPageRow(int pageRow) {
		this.pageRow = pageRow;
	}
	public boolean isPageNext() {
		return pageNext;
	}
	public void setPageNext(boolean pageNext) {
		this.pageNext = pageNext;
	}
	public boolean isPagePrev() {
		return pagePrev;
	}
	public void setPagePrev(boolean pagePrev) {
		this.pagePrev = pagePrev;
	}
	
	
	

}
