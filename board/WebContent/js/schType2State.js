/**
 * 스케줄러 type2 상태표시
 */


$(document).ready(function() {
	$.ajax({
		type: "GET",
		url: "/board/Sch02StateServlet",
		success: function(data) {
			document.getElementById('scheduler02').value = data;
		},
		error: function(request, error) {
			alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
		}
	});
});



$(function() {
	$("#sch2Run").click(function() {
		$.ajax({
			type: "GET",
			url: "/board/Sch02StateServlet",
			success: function(data) {
				if (data) {
					document.getElementById('scheduler02').value = '스케쥴러 #2 : Running';
				} else {
					document.getElementById('scheduler02').value = '스케쥴러 #2 : Stopped';
				}
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
	$("#sch2Stop").click(function() {
			document.getElementById("scheduler02").value = '스케쥴러 #2 : Stopped';
	});
});


