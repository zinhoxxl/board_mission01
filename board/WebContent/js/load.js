/**
 * load.js
 */


$(document).ready(function() {
	var param = {};
	param.board_num = getQueryStringObject("board_num");
	$.ajax({
		type	 : 'GET',
		url		 : '/board/LoadServlet',
		data	 : param,
		dataType : 'json',
		success : function(data) {
			getLoad(data);
		},
		error : function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
});


function getQueryStringObject(num) {
	var param = window.location.search.substr(location.search.indexOf("?") + 1).split('&');
	var value = "";
	for (var i = 0; i < param.length; i++) {
		temp = param[i].split("=");
		if ([temp[0]] == num) { value = temp[1]; }
	}
	return value;
}


function getLoad(data) {
	if (data != null) {
		var jsonType 	 = data;
		var boardNum 	 = jsonType.board_num;
		var boardTitle   = jsonType.title;
		var boardContent = jsonType.content;
		var fileName     = jsonType.fileName;
		$('#board_num').val(boardNum);
		$('#title').val(boardTitle);
		$('#content').val(boardContent);
		if (fileName != null) 
		$('#downFile').val(fileName);
	}

}


function fileDownload() {
	var downFile = $('#downFile').val();
	var fileName = encodeURI(downFile);
	if (fileName == "") {
		alert("등록된 첨부파일이 없습니다");
		history.back();
	} 
	else {
		window.location = '/board/DownloadServlet?fileName=' + fileName;
	}
}

