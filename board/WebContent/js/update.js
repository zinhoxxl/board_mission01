/**
 * update.js
 */


$(document).on('click', '#update', function() {
	var updateForm = $('#updateForm')[0];
	var formData = new FormData(updateForm);
	$.ajax({
		type: 'POST',
		url: '/board/UpdateServlet',
		data: formData,
		processData: false,
        contentType: false,
		dataType: 'json',
		success: function() {
			alert("수정 완료");
			window.close();
			opener.location.href = './board.html';
		},
		error: function(request, error) {
			alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
		}
	});
});
