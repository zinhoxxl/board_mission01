/**
 * 스케줄러 type1
 */


/* type1 RUN */
$(function() {
	$('#sch1Run').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch01RunServlet',
			success: function() {
				alert('스케줄러#1 실행');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});


/* type1 STOP */
$(function() {
	$('#sch1Stop').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch01StopServlet',
			success: function() {
				alert('스케줄러#1 중지');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});


