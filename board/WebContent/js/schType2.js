/**
 * 스케줄러 type2
 */


/* type2 RUN */
$(function() {
	$('#sch2Run').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch02RunServlet',
			success: function() {
				alert('스케줄러#2 실행');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});


/* type2 STOP */
$(function() {
	$('#sch2Stop').click(function() {
		$.ajax({
			type: 'GET',
			url: '/board/Sch02StopServlet',
			success: function() {
				alert('스케줄러#2 중지');
			},
			error: function(request, error) {
				alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
			}
		});
	});
});


